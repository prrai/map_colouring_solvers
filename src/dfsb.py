# CSE 537.01: Artificial Intelligence - Assignment 2
# dfsb.py - Depth first search with backtracking.
# Submitted By: Prasoon Rai
# SBU ID: 110921754
import sys
import copy
import time
from collections import deque

# Global variables to determine metrics
count_steps = 0
arc_consistency_calls = 0


def parse_input_file_and_initialize_attributes(input_file):
    """
    Function to parse the input file and construct constraint graph and other data structures
    :param input_file: Input file from command line
    :return: All the data structures filled after parsing the input file
    """
    input_data = ''
    edge_tuple = ()
    try:
        with open(input_file, mode='r') as data:
            input_data = data.read()
    except EnvironmentError:
        # If file can't be opened
        print("\n[ERROR] Unable to read input file, or it does not exist. Aborting!\n")
    input_data = input_data.split('\n')
    variables, constraints, colors = input_data[0].split('\t')
    constraint_graph = [[0] * int(variables) for _ in range(int(variables))]
    input_data.remove(input_data[0])
    for record in input_data:
        if len(record.split('\t')) == 2:
            edge_from, edge_to = record.split('\t')
            edge_tuple = edge_tuple + ([int(edge_from), int(edge_to)], )
            # The graph is undirected
            constraint_graph[int(edge_from)][int(edge_to)] = 1
            constraint_graph[int(edge_to)][int(edge_from)] = 1
    return constraint_graph, variables, colors, edge_tuple


def can_assign_current_color(current_variable, constraint_graph, variables, assignments, color):
    """
    Check if we can assign the current color to the current variable, based on neighbors' colors
    """
    for variable in range(int(variables)):
        # Check for constraints
        if constraint_graph[current_variable][variable] == 1 and color == assignments[variable]:
            return False
    return True


def recurse_simple_depth_first_search_backtracking(constraint_graph, colors, variables, assignments,
                                                   current_variable, start_time):
    """
    Recursive function to execute naive depth first search with back tracking to solve the CSP
    """
    global count_steps
    elapsed_time = time.time() - start_time
    # Try for upto 60 seconds
    if int(elapsed_time) > 60:
        return False
    if int(variables) == current_variable:
        return True
    for color in range(int(colors)):
        # For each color, check if we can assign it to the current variable
        if can_assign_current_color(current_variable, constraint_graph, variables, assignments, color) is True:
            assignments[current_variable] = color
            count_steps += 1
            # Recursive call
            result = recurse_simple_depth_first_search_backtracking(
                constraint_graph, colors, variables, assignments, int(current_variable) + 1, start_time)
            if result is True:
                return True
            # Back track to get the earlier state
            assignments[current_variable] = -1
    return False


def get_most_constrained_variable(colors, assignments, available_colors_dict):
    """
    Get the most constrained variable (MRV)
    """
    minimum_list = int(colors) + 1
    most_constrained_variable = -1
    for key, value in available_colors_dict.items():
        if assignments[key] == -1:
            # Get the variable that has minimum possible color assignments possible
            if minimum_list > len(value):
                minimum_list = len(value)
                most_constrained_variable = key
    return most_constrained_variable


def get_least_constraining_color_order_list(current_variable, constraint_graph, colors, available_colors_dict):
    """
    Get the least constraining value (LCV)
    """
    least_constraining_color_dict = {}
    # If there is no possible color, return
    if len(available_colors_dict[current_variable]) < 1:
        return []
    for color in available_colors_dict[current_variable]:
        # For all the colors in the possible colors available to the current variable:
        temp_dict = {}
        for index, item in enumerate(constraint_graph[int(current_variable)]):
            if item == 1:
                # Slice out the neighbours, we are going to remove some colors from domain
                # We don't want to affect the original dictionary
                temp_dict[index] = copy.deepcopy(available_colors_dict[index])
        for value in temp_dict.values():
            # Remove the color from neighbours
            if color in value:
                value.remove(color)
        if len(temp_dict) > 0:
            # Get the number of assignable colors for most constrained neighbour if we choose the current color
            minimum_list = int(colors) + 1
            for key, value in temp_dict.items():
                if minimum_list > len(value):
                    minimum_list = len(value)
            least_constraining_color_dict[color] = minimum_list
    # Get a descending ordered list of colors which is based off a dictionary that stores the number of colors
    # available to the most constrained variable for that color. Hence we are getting the max of mins here.
    least_constraining_color_order_list = sorted(least_constraining_color_dict,
                                                 key=lambda k: least_constraining_color_dict[k], reverse=True)
    return least_constraining_color_order_list


def arc_consistency(constraint_graph, available_colors_dict, edge_tuple):
    """
    AC3 implementation
    """
    global arc_consistency_calls
    edge_pairs_queue = deque(edge_tuple)
    while edge_pairs_queue:
        # Get edge pairs to a queue
        tail, head = edge_pairs_queue.popleft()
        # Prune the domain of tail variable if it causes conflict to the head
        if remove_inconsistent(tail, head, available_colors_dict):
            arc_consistency_calls += 1
            # If the tail has no color left after pruning, we have reached an inconsistent state, return False
            if len(available_colors_dict[tail]) == 0:
                return False
            for index, item in enumerate(constraint_graph[tail]):
                if item == 1:
                    edge_pairs_queue.append((tail, index))
    return True


def remove_inconsistent(tail, head, available_colors_dict):
    """
    Remove inconsistent color from tail that can starve the head of any possible color
    """

    prune_domain = False
    # If the head can have just one color
    if len(available_colors_dict[head]) == 1:
            # And that color is same as tail's
            if available_colors_dict[head][0] in available_colors_dict[tail]:
                # Remove it
                available_colors_dict[tail].remove(available_colors_dict[head][0])
                prune_domain = True
    return prune_domain


def recurse_depth_first_search_backtracking_improved(constraint_graph, colors,
                                                     variables, assignments, available_colors_dict,
                                                     edge_tuple, start_time):
    """
    Improved DFS recursive loop
    """
    global count_steps
    elapsed_time = time.time() - start_time
    # Return if the CSP is not solved in 60 seconds
    if int(elapsed_time) > 60:
        return False
    # Get MRV
    most_constrained_variable = int(get_most_constrained_variable(colors, assignments, available_colors_dict))

    # Exit the recursion with solution if no MRV is found
    if most_constrained_variable == -1:
        return True
    # Get LCV
    least_constraining_color_order_list = get_least_constraining_color_order_list(int(most_constrained_variable),
                                                                                  constraint_graph,
                                                                                  colors, available_colors_dict)
    if len(least_constraining_color_order_list) > 0:
        old_domain_dict = copy.deepcopy(available_colors_dict)
        # For each color in the priority list of LCV for MRV:
        for color in least_constraining_color_order_list:
            assignments[most_constrained_variable] = color
            # Check arc consistency
            arc_consistency_result = arc_consistency(constraint_graph, available_colors_dict, edge_tuple)
            # If arc consistency result is False, we don't enter the recursion
            # We need to revert the changes made to the domain, so we can backtrack
            if arc_consistency_result is False:
                available_colors_dict = copy.deepcopy(old_domain_dict)
                assignments[most_constrained_variable] = -1
                continue
            # Back up the  state
            for index, item in enumerate(constraint_graph[most_constrained_variable]):
                if item == 1 and color in available_colors_dict[index]:
                    available_colors_dict[index].remove(color)
            old_domain_list = available_colors_dict[most_constrained_variable][:]
            available_colors_dict[most_constrained_variable] = [-1 for _ in range(int(colors))]
            count_steps += 1
            # Recursive call
            result = recurse_depth_first_search_backtracking_improved(constraint_graph, colors, variables,
                                                                      assignments, available_colors_dict,
                                                                      edge_tuple, start_time)
            if result is True:
                return True
            # revert state since we got a false
            for index, item in enumerate(constraint_graph[most_constrained_variable]):
                if item == 1 and assignments[index] == -1 and color not in \
                        available_colors_dict[index]:
                    available_colors_dict[index].append(color)
            available_colors_dict[most_constrained_variable] = old_domain_list[:]
            assignments[most_constrained_variable] = -1
            available_colors_dict = copy.deepcopy(old_domain_dict)
    return False


def depth_first_search_backtracking(constraint_graph, variables, colors, optimized, output_file, edge_tuple):
    """
    Wrapper function for the two DFS approaches, triggers one based on the mode flag in command line
    """
    global count_steps
    assignments = [-1 for _ in range(int(variables))]
    if optimized is True:
        # Run DFSB++
        available_colors_dict = {}
        for variable in range(int(variables)):
            available_colors_dict[variable] = [_ for _ in range(int(colors))]
        start_time = time.time()
        result = recurse_depth_first_search_backtracking_improved(constraint_graph, colors,
                                                                  variables, assignments,
                                                                  available_colors_dict, edge_tuple, start_time)
    else:
        # Run DFSB naive
        start_time = time.time()
        result = recurse_simple_depth_first_search_backtracking(constraint_graph, colors, variables,
                                                                assignments, 0, start_time)
    try:
        # Write results to output file
        with open(output_file, "w") as output_data:
            elapsed_time = time.time() - start_time
            if result is False:
                print("\n[ERROR] Unable to find a valid color assignment for the provided input set, Exiting!\n")
                output_data.write("No answer")
            else:
                print(assignments)
                for assignment in assignments:
                    output_data.write("{0}\n".format(int(assignment)))
            print("Solution run time: %2f seconds" % (float(elapsed_time)))
            print("Number of search steps: {0}".format(count_steps))
            print("Arc consistency pruning calls: {0}".format(arc_consistency_calls))
    except EnvironmentError:
        print("\n[ERROR] Unable to create output file. Aborting!\n")


def main():
    # Validate the commmand line
    if len(sys.argv) < 4:
        print("\n[ERROR] Incomplete arguments, abort!\n")
        exit(-1)
    input_file = sys.argv[1]
    output_file = sys.argv[2]
    mode_flag = sys.argv[3]
    # Fill data structures from input file
    constraint_graph, variables, colors, edge_tuple = parse_input_file_and_initialize_attributes(input_file)
    # Run the algorithm based on mode flag
    if mode_flag == '0':
        print("\n[NOTE] Executing plain dfsb since mode_flag is 0\n")
        depth_first_search_backtracking(constraint_graph, variables, colors, False, output_file, edge_tuple)
    else:
        print("\n[NOTE] Executing improved dfsb since mode_flag is 1\n")
        depth_first_search_backtracking(constraint_graph, variables, colors, True, output_file, edge_tuple)


if __name__ == '__main__':
    # Execute main()
    main()
