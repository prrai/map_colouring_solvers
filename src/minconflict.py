# CSE 537.01: Artificial Intelligence - Assignment 2
# minconflict.py - Minconflict algorithm.
# Submitted By: Prasoon Rai
# SBU ID: 110921754
import sys
import time
from random import choice, randrange


def parse_input_file_and_initialize_attributes(input_file):
    """
    Function to parse the input file and construct constraint graph and other data structures
    :param input_file: Input file from command line
    :return: All the data structures filled after parsing the input file
    """
    input_data = ''
    try:
        with open(input_file, mode='r') as data:
            input_data = data.read()
    except EnvironmentError:
        print("\n[ERROR] Unable to read input file, or it does not exist. Aborting!\n")
    input_data = input_data.split('\n')
    variables, constraints, colors = input_data[0].split('\t')
    constraint_graph = [[0] * int(variables) for _ in range(int(variables))]
    input_data.remove(input_data[0])
    for record in input_data:
        if len(record.split('\t')) == 2:
            edge_from, edge_to = record.split('\t')
            constraint_graph[int(edge_from)][int(edge_to)] = 1
            constraint_graph[int(edge_to)][int(edge_from)] = 1
    return constraint_graph, variables, colors


def get_random_configuration(assignment, colors):
    return [randrange(0, int(colors)) for _ in range(0, len(assignment))]


def get_random_from_set(constrained_variables_set):
    # Get a random value from a set
    return choice(tuple(constrained_variables_set))


def get_constrained_variables_set(assignments, constraint_graph, variables):
    # Get the set of constrained variables for the current state assignment
    constrained_variables_set = set()
    for variable in range(int(variables)):
        for index, item in enumerate(constraint_graph[int(variable)]):
            if item == 1 and assignments[index] == assignments[variable]:
                constrained_variables_set.add(variable)
    return constrained_variables_set


def get_best_color_for_random_constrained_var(random_constrained_var, assignments, constraint_graph, variables, colors):
    # Determine the best color for the chosen variable
    minimum_conflicted_neighbours = int(variables) + 1
    equally_like_colors = set()
    for color in range(int(colors)):
        conflicts_for_current_color = 0
        for index, item in enumerate(constraint_graph[int(random_constrained_var)]):
            if item == 1 and assignments[index] == color:
                # Increase the count of conflicts
                conflicts_for_current_color += 1
        if minimum_conflicted_neighbours >= conflicts_for_current_color:
            # If there are equally likely colors, add them to the list
            minimum_conflicted_neighbours = conflicts_for_current_color
            equally_like_colors.add(color)
    # Break the ties randomly
    best_color = get_random_from_set(equally_like_colors)
    return best_color


def solve_csp_min_conflict(constraint_graph, variables, colors, output_file, assignments, limit):
    # Main solver for min conflict algorithm
    result = False
    # Get a random configuration
    assignments = get_random_configuration(assignments, colors)
    # Start time slice timer and over-all timer
    overall_start_time = time.time()
    start_time = time.time()
    # Iterate for the max limit of 500000
    for iterate in range(limit):
        overall_elapsed_time = time.time() - overall_start_time
        # If 60 seconds are elapsed, exit
        if int(overall_elapsed_time) > 60:
            print("Time Limit of 60 seconds exceeded!")
            print("Steps tried before exit: {0}".format(iterate))
            result = False
            break
        elapsed_time = time.time() - start_time
        if int(elapsed_time) == 5:
            # Get a random configuration after time slice of 5 seconds
            assignments = get_random_configuration(assignments, colors)
            start_time = time.time()
        # Get the set of constrained variables
        constrained_variables_set = get_constrained_variables_set(assignments, constraint_graph, variables)
        if len(constrained_variables_set) == 0:
            # If the set is empty, we have a solution
            print("Solution found in %f seconds!" % (float(overall_elapsed_time)))
            print("Solution steps: {0}".format(iterate))
            result = True
            break
        # Choose one constrained variable randomly
        random_constrained_var = get_random_from_set(constrained_variables_set)
        # Get the best color for the chosen variable, the one that constrains the neighbours the least
        best_color_for_var = get_best_color_for_random_constrained_var(random_constrained_var,
                                                                       assignments, constraint_graph, variables, colors)
        # Assign the color to the chose var and repeat
        assignments[random_constrained_var] = best_color_for_var
    try:
        # Write output to file
        with open(output_file, "w") as output_data:
            if result is False:
                print(
                    "\nUnable to find a valid color assignment for the provided input set, Exiting!\n")
                output_data.write("No answer")
            else:
                print(assignments)
                for assignment in assignments:
                    output_data.write("{0}\n".format(int(assignment)))
    except EnvironmentError:
        print("\n[ERROR] Unable to create output file. Aborting!\n")


def main():
    # Parse command line
    if len(sys.argv) < 3:
        print("\n[ERROR] Incomplete arguments, abort!\n")
        exit(-1)
    input_file = sys.argv[1]
    output_file = sys.argv[2]
    # Fill data structures from input file
    constraint_graph, variables, colors = parse_input_file_and_initialize_attributes(input_file)
    assignments = [-1 for _ in range(int(variables))]
    print("Executing min-conflict algorithm\n")
    print("[NOTE]: The program tries to find the solution till 60 seconds, executing...\n")
    # Solve
    solve_csp_min_conflict(constraint_graph, variables, colors, output_file, assignments, 500000)

if __name__ == '__main__':
    # Main loop
    main()
